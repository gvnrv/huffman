#ifndef CLEANUP_H
#define CLEANUP_H

#define FREE(ptr)       \
do {                    \
    if (ptr != NULL) {  \
        free(ptr);      \
        ptr = NULL;     \
    }                   \
} while(0)

#define FREE_ARRAY(ptr,size)        \
do {                                \
    for(int i = 0; i < size; i++)   \
        FREE(ptr[i]);               \
    FREE(ptr);                      \
} while(0)

#endif
