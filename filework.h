#ifndef FILEWORK_H
#define FILEWORK_H

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>

#include "tree.h"
#include "cleanup.h"

#define NODE -1
#define ROOT -2


tree_node **count_frequencies(FILE *, int *);

void write_tree(tree_node *, int, FILE *);
tree_node *read_tree(FILE *);

void write_compressed_data(tree_node **, int64_t, FILE *, FILE *);
void write_extracted_data(tree_node *, int64_t, FILE *, FILE *);

#endif
