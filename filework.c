#include "filework.h"


#define BUF_SIZE 65536 


/* countig number of appearences of each symbol */
tree_node **count_frequencies(FILE *f, int *size) {

    tree_node **stats;
    char buf[BUF_SIZE];
    
    // determine file size
    fseek(f, 0, SEEK_END);
    *size = ftell(f);
    fseek(f, 0, SEEK_SET);

    // allocating array of pointers on struct pointers
    if (!(stats = calloc(256, sizeof(tree_node *)))) {

        printf("Calloc error! Can't allocate 'stats' array\n");
        return NULL;
    }

    //allocating memory for each element
    int r = 0, readed = 0;
    for (int i = 0; i < 256; i++) {
    
        if(!(stats[i] = calloc(1, sizeof(tree_node)))) {
            printf("calloc error! Can't allocate 'stats[%d]'\n", i);
            return NULL;
        }
        stats[i]->code = i;

        // just in case
        stats[i]->l = NULL;
        stats[i]->r = NULL;
    }

    // reading characters from file
    for ( ; readed < *size ; ) {

        r = fread(&buf, sizeof(char), BUF_SIZE, f);
        readed += r;

        for (int i = 0; i < r; i++) {

            stats[(int)(buf[i]) + 128]->f++;
        }
    }

    return stats;
}


/* auxillary recursive function for writing tree to a file */
void write_tree_aux(tree_node *node, FILE *f) {

    fwrite(&(node->code), sizeof(node->code), 1, f);
    fwrite(&(node->index), sizeof(node->index), 1, f);

    if (node->l != NULL) {

        write_tree_aux(node->l, f);
    }

    if (node->r != NULL) {

        write_tree_aux(node->r, f);
    }
}


/* writes tree in recognizable format */
void write_tree(tree_node *root, int size, FILE *f) {

    // write size \ index tree \ write tree to a file recursively
    fwrite(&size, sizeof(int), 1, f);
    write_tree_aux(root, f);
}


/* reads tree from file */
tree_node *read_tree(FILE *f) {

    int size = 0;
    uint16_t *s_code_index;

    tree_node *root;

    // read tree size
    fread(&size, sizeof(int), 1, f);

    if (size < 0) {
        printf("Unrecognizable format!\n");
        return NULL;
    }

    if (!(s_code_index = malloc(sizeof(uint16_t) * size * 2))) {

        printf("malloc error! Can't allocate code_index array of size %d\n", size);
        return NULL;
    }

    if (!(root = calloc(1, sizeof(tree_node)))) {

        printf("calloc error! Can't allocate 'root' node!\n");
        return NULL;
    }

    // read [code : index] values
    fread(s_code_index, sizeof(uint16_t), size * 2, f);

    root->code = (int16_t)s_code_index[0];
    root->index = s_code_index[1];

    // adding branches to tree
    for (int i = 2; i < size * 2; i += 2) {

        add_node(root, s_code_index[i + 1], (int16_t)s_code_index[i]);
    }

    FREE(s_code_index);
    return root;
}


/* read from input file / encodes and writes huffman codes to a file */
void write_compressed_data(tree_node **leaves, int64_t file_size, FILE *input, FILE *output) {

    printf("COMPRESSING DATA...\n");
    
    int i = 0, j = 0;

    int OUT_BUFFER[BUF_SIZE] = {0};
    char IN_BUFFER[BUF_SIZE] = {0};
    
    int r, mask, path;
    int64_t readed = 0;

    // set file pointer to beginning of file
    rewind(input);

    if (file_size <= 0) {
    
        printf("Zero length file\n");
        return;
    }

    // write file size in file
    fwrite(&file_size, sizeof(int64_t), 1, output);

    // read first portion of data to be coded
    r = fread(&IN_BUFFER, sizeof(char), BUF_SIZE, input);

    // picking a first symbol from buffer
    tree_node *symbol = leaves[(int)(IN_BUFFER[j++]) + 128];

    mask = symbol->level;
    path = symbol->path;

    readed++;

    do {

        for (int offset = 0; offset < 32; offset++) {
    
            if (mask < 0) {

                // picking next symbol from buffer
                symbol = leaves[(int)(IN_BUFFER[j++]) + 128];
                mask = symbol->level;
                path = symbol->path;

                readed++;

                // all readed symbols were processed / read new portion
                if (j == r) {
                    r = fread(&IN_BUFFER, sizeof(char), BUF_SIZE, input);
                    j = 0;
                }

                if (readed > file_size) {
                    break;
                }
            }

            // setting buffer bits according to a path bits
            OUT_BUFFER[i] |= ((path >> (mask--)) & 1) << offset;
        }
        i++;

        if (i == BUF_SIZE) {

            fwrite(OUT_BUFFER, sizeof(int), BUF_SIZE, output);
            i = 0;
        }

        // clearing buffer content
        OUT_BUFFER[i] = 0;
    } while (readed <= file_size);

    // write all the rest
    fwrite(OUT_BUFFER, sizeof(int), i, output);
}


/* reads enocoded data / decodes it / writes to another file */
void write_extracted_data(tree_node *tree, int64_t fsize, FILE *input, FILE *output) {

    printf("EXTRACTING DATA...\n");

    int IN_BUFFER[BUF_SIZE];
    char OUT_BUFFER[BUF_SIZE];

    int64_t written = 0;
    tree_node *node = tree;
    
    int r;
    int i = 0, j = 0;

    // read a first portinon of data
    r = fread(IN_BUFFER, sizeof(int), BUF_SIZE, input);
    
    do {

        for (int offset = 0; offset < 32; offset++) {

            // descenting down the tree to find a leave
            if (node->code < 0) {

                node = (IN_BUFFER[i] & (1 << offset)) ? node->r : node->l;

            } else {
 
                // leave achieved
                // writing leave value to output buffer           
                OUT_BUFFER[j++] = (char)(node->code - 128);
                written++;
                
                // starting from a root
                node = (IN_BUFFER[i] & (1 << offset)) ? tree->r : tree->l;
                
                // write decoded portion of data to file 
                if (j == BUF_SIZE) {
                    fwrite(OUT_BUFFER, sizeof(char), BUF_SIZE, output);
                    j = 0;
                }

                if (written == fsize)
                    break;
            }
        }
        i++;

        if (j == BUF_SIZE) {
            fwrite(OUT_BUFFER, sizeof(char), BUF_SIZE, output);
            j = 0;
        }

        // read new portion of data
        if (i == r) {
        
            r = fread(IN_BUFFER, sizeof(int), BUF_SIZE, input);
            i = 0;
        }
    } while (written < fsize);

    // write all the rest
    fwrite(OUT_BUFFER, sizeof(char), j, output);
}
