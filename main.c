#include <stdio.h>
#include <string.h>


#include "huffman.h"


#define USG "usage: ./main.c <options> <input file> <output file>\n"
#define HELP "\n\t-x - eXtract\n\t-c - compress\n\t-h - this help\n\n"


int main(int argc, char **argv){

    if (argc < 4) {
        
        printf(USG HELP);
        return 1;    
    }

    if (strcmp(argv[1], "-x") == 0)
        extract(argv[2], argv[3]);

    else if (strcmp(argv[1], "-c") == 0)
        compress(argv[2], argv[3]);

    else if (strcmp(argv[1], "-h") == 0)
        printf(USG HELP);

    return 0;
}
