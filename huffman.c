#include "huffman.h"


/* compare function for qsort */
int sort_nodes(const void *a, const void *b) {

    const tree_node *tna = *(const tree_node **)a;
    const tree_node *tnb = *(const tree_node **)b;

    return ((tna->f > tnb->f) - (tna->f < tnb->f));

}

/* compress function */
void compress(char *input_file, char *output_file) {

    FILE *f1, *f2;

    tree_node **stats, **trunc_stats, *root;
    tree_node **huffman_table;

    int file_size = 0, tree_size = 0, leaves_size = 0; 

    if (!(huffman_table = calloc(256, sizeof(tree_node *)))) {

        printf("Unable to lacate 'huffman_table' of size %d", 256);
        return;
    }

    if (!(f1 = fopen(input_file, "rb"))) {
    
        printf("Unable to open file %s for read!\n", input_file);
        FREE(huffman_table);        
        return;    
    }

    if (!(f2 = fopen(output_file, "wb"))) {
    
        printf("Unable to open file %s for write!\n", output_file);

        fclose(f1);
        FREE(huffman_table);        

        return;    
    }

    // getting frequency statistics and size of file
    stats = count_frequencies(f1, &file_size);

    // copying pointers from stats to prevent loosing their order
    for (int i = 0; i < 256; i++) {
        huffman_table[i] = stats[i];
    }

    // shift pointer to non-zero stats and build tree
    trunc_stats = offset_zeros(stats, 256, &leaves_size);
    root = create_huffman_tree(trunc_stats, leaves_size, &tree_size);

    if (!root) {
        
        printf("Can't create tree!\n");
 
        FREE_ARRAY(stats, 256 - leaves_size);
        FREE(huffman_table);

        fclose(f1);
        fclose(f2);
       
        return;
    }

    index_huffman_tree(root, -1, 0);
    
    // write it all
    write_tree(root, leaves_size + tree_size, f2);
    write_compressed_data(huffman_table, file_size, f1, f2);

    // cleanup
    FREE_ARRAY(stats, 256 - leaves_size);
    FREE(huffman_table);
    
    clear_tree(root);

    fclose(f1);
    fclose(f2);

}


/* extract function */
void extract(char *input_file, char *output_file) {

    FILE *f1, *f2;
    tree_node *tree;

    int64_t file_size = 0;

    if (!(f1 = fopen(input_file, "rb"))) {
    
        printf("Unable to open %s for read!\n", input_file);
        return;
    }

    if (!(f2 = fopen(output_file, "wb"))) {

        printf("Unable to open %s for read!\n", input_file);
        fclose(f1);        
        return;
    }

    // read it all
    tree = read_tree(f1);

    if (!tree) {

        printf("Can't create tree!\n");

        fclose(f1);
        fclose(f2);
        return;
    }

    fread(&file_size, sizeof(int64_t), 1, f1);
    write_extracted_data(tree, file_size, f1, f2);

    // cleanup
    clear_tree(tree);

    fclose(f1);
    fclose(f2);
}


/* indexes each element with unique number and saves path from root 
too leave and level number / fibonacci will overflow and kill it */
void index_huffman_tree(tree_node *node, int level, int path) {

    static short index = 1;

    if (node->l != NULL) {
    
        index_huffman_tree(node->l, level + 1, (path << 1));
    }

    node->index = index;
    index++;

    if (node->r != NULL) {
        
        index_huffman_tree(node->r, level + 1, (path << 1) + 1);
    }
 
    node->level = level;
    node->path = path;
}


/* recursive huffman tree constructing */
tree_node *create_huffman_tree(tree_node **stats, int size, int *nodes_count) {

    if (!stats) {

        printf("stats array is NULL\n");
        return NULL;
    }

    qsort(stats, size, sizeof(tree_node *), sort_nodes);
    tree_node *node = calloc(1, sizeof(tree_node));

    node->f = stats[0]->f + stats[1]->f;
    node->code = -1;

    node->l = stats[0];
    node->r = stats[1];
    
    stats[0]->prev = node;
    stats[1]->prev = node;

    stats[0]->dir = 0;
    stats[1]->dir = 1;

    (*nodes_count)++;
    stats[1] = node;

    if (size > 2) {

        return create_huffman_tree(++stats, size - 1, nodes_count);
    
    } else {
    
        ++stats;
        stats[0]->code = -2;
        stats[0]->prev = NULL;
        stats[0]->dir = -1;
        
        return *(stats);
    }
}


/* shifts pointer to a first nonzero value in array */
tree_node **offset_zeros(tree_node **stats, int size, int *new_size) {

    int i = 0;

    qsort(stats, size, sizeof(tree_node *), sort_nodes);

    do {

        if (stats[i++]->f != 0) {

            (*new_size) = size - i + 1;
            return &(stats[i - 1]);
        }
    } while(i < size);

    printf("All zeros! Nothing to do!\n");
    return NULL;
}
