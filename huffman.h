#ifndef HUFFMAN_H
#define HUFFMAN_H

#include "tree.h"
#include "filework.h"
#include "cleanup.h"


#include <inttypes.h>
#include <string.h>


#define PFX ".hf"


tree_node **offset_zeros(tree_node **, int, int *);
tree_node *create_huffman_tree(tree_node **, int, int *);
void index_huffman_tree(tree_node *, int, int);

void compress(char *, char *);
void extract(char *, char *);
#endif
