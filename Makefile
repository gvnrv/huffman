CC=gcc
CFLAGS=-c -Wall -Werror -std=c99
LDFLAGS=
SOURCES=main.c huffman.c tree.c filework.c
OBJECTS=$(SOURCES:.c=.o)
EXECUTABLE=hf

hf: $(SOURCES) $(OBJECTS)
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@

.c.o:
	$(CC) $(CFLAGS) $< -o $@

clean:
	rm *.o
