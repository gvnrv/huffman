#include "tree.h"


/* add node auxillary recursive function */
void add_node_aux(tree_node *prev_root, tree_node *root, uint16_t index, int16_t code, int dir) {

    if (root && index < root->index) {

        add_node_aux(root, root->l, index, code, 0);
        return;
    
    }
        if (root && index > root->index) {    
        add_node_aux(root, root->r, index, code, 1);
        return;
    }

    tree_node *node = calloc(1, sizeof(tree_node));
    
    node->code = code;
    node->index = index;
    node->prev = prev_root;
    node->dir = dir;

    if (dir == 1) {

        prev_root->r = node;
    
    } else if (dir == 0) {

        prev_root->l = node;
    }
}


/* add node to tree */
void add_node(tree_node *root, uint16_t index, int16_t code){

    if (code == root->code) {
        return;

    } else if (index < root->index) {
        
        add_node_aux(root, root->l, index, code, 0);

    } else if (index > root->index) {
        
        add_node_aux(root, root->r, index, code, 1);
    }    
}


/* recursively clear tree */
int clear_tree(tree_node *root) {

    if (!root) {

        return 1;
    }

    if (root->code > -1) {
                        
        FREE(root);
        return 1;
    
    } else if (clear_tree(root->l) && clear_tree(root->r)) {

        FREE(root);
        return 1;
    }   

    return 0;
}
