#ifndef TREE_H
#define TREE_H


#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "cleanup.h"

/* tree node structure */
typedef struct tree_node {

    int64_t f;
    
    struct tree_node *l;
    struct tree_node *r;
    struct tree_node *prev;

    int16_t code;
    uint16_t index;

    int dir;
    int level;
    int path;

} tree_node;


void add_node(tree_node *, uint16_t, int16_t);
int clear_tree(tree_node *);

#endif
